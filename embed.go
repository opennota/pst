// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"crypto/md5" //nolint:gosec
	"embed"
	"encoding/hex"
	"io"
	"io/fs"
	"path"
	"strings"
)

type FS struct {
	underlying fs.FS
	fmap       map[string]string
	rmap       map[string]string
}

//go:embed static/*.css static/*.js
var assets embed.FS

//go:embed index.html
var templateData string

func NewFS(underlying fs.FS) *FS {
	fmap := make(map[string]string)
	rmap := make(map[string]string)
	_ = fs.WalkDir(underlying, ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}
		if strings.HasSuffix(path, ".js") || strings.HasSuffix(path, ".css") {
			h := md5.New() //nolint:gosec
			f, _ := underlying.Open(path)
			defer f.Close()
			_, _ = io.Copy(h, f)
			newPath := addSuffix(path, hex.EncodeToString(h.Sum(nil)))
			fmap[newPath] = path
			rmap[path] = newPath
		} else {
			fmap[path] = path
			rmap[path] = path
		}
		return nil
	})
	return &FS{
		underlying: underlying,
		fmap:       fmap,
		rmap:       rmap,
	}
}

func (fs *FS) Open(name string) (fs.File, error) {
	name = strings.TrimPrefix(name, "/")
	if mapped := fs.fmap[name]; mapped != "" {
		name = mapped
	}
	return fs.underlying.Open(name)
}

func addSuffix(filename, suffix string) string {
	ext := path.Ext(filename)
	return strings.TrimSuffix(filename, ext) + "." + suffix + ext
}

func (fs *FS) Map(path string) string {
	return "/" + fs.rmap[strings.TrimPrefix(path, "/")]
}

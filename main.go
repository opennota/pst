// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"compress/gzip"
	"encoding/csv"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"html"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/opennota/pst/objectid"
	"go.etcd.io/bbolt"
)

type App struct {
	db           *bbolt.DB
	indexTmpl    *template.Template
	notesPerPage int
}

type Note struct {
	ID      string    `json:"id"`
	Text    string    `json:"text"`
	Created time.Time `json:"created"`
	Tags    []string  `json:"tags"`
	Marks   []string  `json:"marks"`
}

var errNotFound = errors.New("not found")

func marshal(b *bbolt.Bucket, key []byte, val interface{}) error {
	data, _ := json.Marshal(val)
	return b.Put(key, data)
}

func unmarshal(b *bbolt.Bucket, key []byte, val interface{}) (bool, error) {
	data := b.Get(key)
	if data == nil {
		return false, nil
	}
	if err := json.Unmarshal(data, val); err != nil {
		return true, err
	}
	return true, nil
}

func httpError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

func (app *App) index(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	before := r.FormValue("before")
	id, _ := objectid.NewFromHex(before)

	var notes []Note
	if err := app.db.View(func(tx *bbolt.Tx) error {
		notes = notes[:0]
		b := tx.Bucket([]byte("index"))
		c := b.Cursor()
		var k, v []byte
		if !id.IsZero() {
			k, v = c.Seek(id[:])
		} else {
			k, v = c.Last()
		}
		for count := 0; count <= app.notesPerPage && k != nil; k, v = c.Prev() {
			var note Note
			if err := json.Unmarshal(v, &note); err != nil {
				return err
			}
			notes = append(notes, note)
			count++
		}
		return nil
	}); err != nil {
		log.Println("failed to get notes:", err)
		httpError(w, http.StatusInternalServerError)
		return
	}

	newBefore := ""
	if len(notes) > app.notesPerPage {
		newBefore = notes[app.notesPerPage].ID
		notes = notes[:app.notesPerPage]
	}

	if err := app.indexTmpl.Execute(w, struct {
		Notes  []Note
		Before string
	}{
		notes,
		newBefore,
	}); err != nil {
		log.Println(err)
	}
}

func (app *App) addNote(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}

	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		log.Println("failed to decode JSON:", err)
		httpError(w, http.StatusBadRequest)
		return
	}

	tags := note.Tags[:0]
	for _, tag := range note.Tags {
		tag = strings.TrimSpace(tag)
		if tag == "" {
			continue
		}
		tags = append(tags, tag)
	}
	note.Tags = tags

	var id objectid.ObjectID
	if note.ID == "" {
		id = objectid.New()
		note.ID = id.Hex()
	} else {
		var err error
		id, err = objectid.NewFromHex(note.ID)
		if err != nil {
			log.Println("failed to decode ObjectID:", err)
			httpError(w, http.StatusBadRequest)
			return
		}
	}

	if note.Created.IsZero() {
		note.Created = time.Now()
	}

	if err := app.db.Update(func(tx *bbolt.Tx) error {
		return marshal(tx.Bucket([]byte("index")), id[:], note)
	}); err != nil {
		log.Println("failed to write the note to the database:", err)
		httpError(w, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		ID string `json:"id"`
	}{
		note.ID,
	}); err != nil {
		log.Println(err)
	}
}

func appendUniq(a []string, s string) []string {
	for _, v := range a {
		if v == s {
			return a
		}
	}
	return append(a, s)
}

func toJSON(a []string) string {
	if len(a) == 0 {
		return ""
	}
	data, _ := json.Marshal(a)
	return string(data)
}

func (app *App) addTag(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}

	var postData struct {
		ID  string
		Tag string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println(err)
		httpError(w, http.StatusBadRequest)
		return
	}

	id, err := objectid.NewFromHex(postData.ID)
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	tag := strings.TrimSpace(postData.Tag)
	if postData.Tag == "" {
		httpError(w, http.StatusBadRequest)
		return
	}

	if err := app.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var note Note
		if found, err := unmarshal(b, id[:], &note); err != nil {
			return err
		} else if !found {
			return errNotFound
		}
		note.Tags = appendUniq(note.Tags, tag)
		return marshal(b, id[:], note)
	}); err != nil {
		if err == errNotFound {
			log.Printf("note id=%s not found", postData.ID)
			http.NotFound(w, r)
		} else {
			log.Printf("failed to add tag %q: %v", tag, err)
			httpError(w, http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) removeTag(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}

	var postData struct {
		ID  string
		Tag string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println("failed to decode JSON:", err)
		httpError(w, http.StatusBadRequest)
		return
	}

	id, err := objectid.NewFromHex(postData.ID)
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}
	tag := postData.Tag

	if err := app.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var note Note
		if found, err := unmarshal(b, id[:], &note); err != nil {
			return err
		} else if !found {
			return errNotFound
		}
		p := note.Tags[:0]
		for _, t := range note.Tags {
			if t == tag {
				continue
			}
			p = append(p, t)
		}
		note.Tags = p
		return marshal(b, id[:], note)
	}); err != nil {
		if err == errNotFound {
			log.Printf("note id=%s not found", postData.ID)
			http.NotFound(w, r)
		} else {
			log.Printf("failed to remove tag %q: %v", tag, err)
			httpError(w, http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) addMark(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}

	var postData struct {
		ID   string
		Mark string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println(err)
		httpError(w, http.StatusBadRequest)
		return
	}

	mark := strings.TrimSpace(postData.Mark)
	if mark == "" {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	id, err := objectid.NewFromHex(postData.ID)
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	if err := app.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var note Note
		if found, err := unmarshal(b, id[:], &note); err != nil {
			return err
		} else if !found {
			return errNotFound
		}
		note.Marks = appendUniq(note.Marks, mark)
		return marshal(b, id[:], note)
	}); err != nil {
		if err == errNotFound {
			log.Printf("note id=%s not found", postData.ID)
			http.NotFound(w, r)
		} else {
			log.Printf("failed to add mark %q: %v", mark, err)
			httpError(w, http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) backup(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		httpError(w, http.StatusMethodNotAllowed)
		return
	}

	var notes []Note
	if err := app.db.View(func(tx *bbolt.Tx) error {
		notes = notes[:0]
		return tx.Bucket([]byte("index")).ForEach(func(k, v []byte) error {
			var note Note
			if err := json.Unmarshal(v, &note); err != nil {
				return err
			}
			notes = append(notes, note)
			return nil
		})
	}); err != nil {
		log.Println("failed to get notes:", err)
		httpError(w, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Encoding", "gzip")
	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="pst-%s-%d.csv"`,
		time.Now().Format("20060102150405"),
		len(notes),
	))

	gw := gzip.NewWriter(w)
	cw := csv.NewWriter(gw)
	for _, n := range notes {
		if err := cw.Write([]string{
			n.Created.Format(time.RFC3339),
			n.ID,
			n.Text,
			toJSON(n.Tags),
			toJSON(n.Marks),
		}); err != nil {
			log.Println("failed to encode notes:", err)
			return
		}
	}
	cw.Flush()

	if err := gw.Close(); err != nil {
		log.Println("failed to encode notes:", err)
	}
}

func main() {
	addr := flag.String("http", ":3000", "HTTP service address")
	notesPerPage := flag.Int("n", 12, "Number of notes per page")
	dbName := flag.String("db", "pst.db", "BoltDB database filename")
	password := flag.String("p", "", "Protect with a password")
	certfile := flag.String("certfile", "", "A certificate for the server")
	keyfile := flag.String("keyfile", "", "A private key for the server")
	log.SetFlags(log.Lshortfile)
	flag.Parse()

	fsys := NewFS(assets)

	indexTmpl, err := template.New("index.html").Funcs(template.FuncMap{
		"static_path": fsys.Map,
		"json": func(a []string) template.HTML {
			if len(a) == 0 {
				return "[]"
			}
			return template.HTML(html.EscapeString(toJSON(a))) //nolint:gosec
		},
	}).Parse(templateData)
	if err != nil {
		log.Fatalln("failed to parse template:", err)
	}

	db, err := bbolt.Open(*dbName, 0o600, nil)
	if err != nil {
		log.Fatalln("failed to open database:", err)
	}

	if err := db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("index"))
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		log.Fatalln("failed to create bucket:", err)
	}

	if flag.NArg() > 0 {
		switch flag.Arg(0) {
		case "import":
			for _, fn := range flag.Args()[1:] {
				if err := importFromCSV(db, fn); err != nil {
					log.Fatal(err)
				}
				log.Println("imported", fn)
			}
		default:
			os.Exit(1)
		}
		return
	}

	app := App{
		db:           db,
		indexTmpl:    indexTmpl,
		notesPerPage: *notesPerPage,
	}

	withAuth := authMiddleware("pst", *password)

	http.HandleFunc("/", withAuth(app.index))
	http.HandleFunc("/add", withAuth(app.addNote))
	http.HandleFunc("/tag/add", withAuth(app.addTag))
	http.HandleFunc("/tag/remove", withAuth(app.removeTag))
	http.HandleFunc("/mark/add", withAuth(app.addMark))
	http.HandleFunc("/backup", withAuth(app.backup))

	fileServer := http.FileServer(http.FS(fsys))
	defaultLastModified := time.Unix(0, 0).Format("Mon, 02 Jan 2006 15:04:05 GMT")
	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Last-Modified", defaultLastModified)
		w.Header().Add("Cache-Control", "max-age: 31536000, immutable")
		fileServer.ServeHTTP(w, r)
	})

	if *certfile != "" && *keyfile != "" {
		log.Fatal(http.ListenAndServeTLS(*addr, *certfile, *keyfile, nil))
	} else {
		log.Println("You won't be able to paste text unless you connect to the server securely (using https).")
		log.Println("Perhaps you forgot to add -certfile and -keyfile options?")
		log.Fatal(http.ListenAndServe(*addr, nil))
	}
}
